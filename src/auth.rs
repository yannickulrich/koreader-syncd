use argon2::{
    password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};
use axum::{
    extract::{State, TypedHeader},
    http::{HeaderMap, Request, StatusCode},
    headers::authorization::{Authorization, Basic},
    middleware::{Next},
    response::{IntoResponse, Json, Response},
};
use rand_core::OsRng;
use serde::{Serialize};
use sqlx::SqlitePool;
use tracing::info;

pub enum Error {
    Internal,
    InvalidRequest,
    Unauthorized,
    UserExists,
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        #[derive(Serialize)]
        struct JsonError {
            code: i32,
            message: &'static str,
        }

        let mut headers = HeaderMap::new();

        let (status, body) = match self {
            Error::Internal => (
                StatusCode::INTERNAL_SERVER_ERROR,
                JsonError {
                    code: 2000,
                    message: "Unknown server error",
                },
            ),
            Error::UserExists => (
                StatusCode::PAYMENT_REQUIRED,
                JsonError {
                    code: 2002,
                    message: "Username is already registered",
                },
            ),
            Error::Unauthorized => (
                StatusCode::UNAUTHORIZED,
                {
                    headers.insert("WWW-Authenticate", "Basic".parse().unwrap());
                    JsonError {
                        code: 2001,
                        message: "Unauthorized",
                    }
                },
            ),
            Error::InvalidRequest => (
                StatusCode::FORBIDDEN,
                JsonError {
                    code: 2003,
                    message: "Invalid request",
                },
            ),
        };

        (status, headers, Json(body)).into_response()
    }
}
pub type Result<T> = std::result::Result<T, Error>;

pub async fn authorize<B>(
    State(pool): State<SqlitePool>,
    headers: HeaderMap,
    auth: Option<TypedHeader<Authorization<Basic>>>,
    request: Request<B>,
    next: Next<B>,
) -> Result<impl IntoResponse> {
    let (username, password) = if let Some(TypedHeader(auth)) = auth {
        (
            auth.username().to_string(),
            format!("{:x}", md5::compute(auth.password().to_string()))
        )
    } else {
        (
            headers
                .get("x-auth-user")
                .ok_or(Error::Unauthorized)?
                .to_str()
                .map_err(|_| Error::InvalidRequest)?
                .to_string(),
            headers
                .get("x-auth-key")
                .ok_or(Error::Unauthorized)?
                .to_str()
                .map_err(|_| Error::InvalidRequest)?
                .to_string(),
        )
    };

    info!(user = username, "authorize");

    let (password_hash,): (String,) =
        sqlx::query_as(r#"SELECT password FROM users WHERE username = ?1"#)
            .bind(username)
            .fetch_optional(&pool)
            .await
            .map_err(|_| Error::Internal)?
            .ok_or(Error::Unauthorized)?;
    let parsed_hash = PasswordHash::new(&password_hash).map_err(|_| Error::Internal)?;

    Argon2::default()
        .verify_password(password.as_bytes(), &parsed_hash)
        .map_err(|_| Error::Unauthorized)?;

    Ok(next.run(request).await)
}

pub async fn create_user(
    pool: SqlitePool,
    username: &String,
    password: &String,
) -> Result<()> {
    info!(user = username, "register");

    let salt = SaltString::generate(&mut OsRng);
    let argon2 = Argon2::default();

    let password = argon2
        .hash_password(password.as_bytes(), &salt)
        .map_err(|_| Error::Internal)?
        .to_string();

    sqlx::query(r#"INSERT INTO users (username, password) VALUES (?1, ?2)"#)
        .bind(&username)
        .bind(&password)
        .execute(&pool)
        .await
        .map_err(|e| match e {
            sqlx::Error::Database(de) if de.code().as_deref() == Some("1555") => Error::UserExists,
            _ => Error::Internal,
        })?;

    Ok(())
}
