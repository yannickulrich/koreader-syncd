use crate::auth;
use serde_json;

use chrono::{
    DateTime,
    NaiveDateTime,
    Utc,
};
use atom_syndication::{
    Category,
    EntryBuilder,
    Entry,
    LinkBuilder,
    Person,
    PersonBuilder,
    TextBuilder,
};
use axum::{
    body::{StreamBody, Bytes, Full},
    extract::{DefaultBodyLimit, Path, State, Multipart},
    http::{HeaderMap, StatusCode},
    middleware::{self},
    response::{IntoResponse, Json, Response},
    routing::{get, post, put},
    Router,
};
use epub::doc::EpubDoc;
use serde::{Serialize};
use sqlx::SqlitePool;
use tracing::info;
use tokio::{
    fs::File,
    io::{AsyncWriteExt},
};
use tokio_util::io::ReaderStream;


pub enum Error {
    NotFound,
    Internal,
    InvalidRequest,
    BookExists,
}

impl IntoResponse for Error {
    fn into_response(self) -> Response {
        #[derive(Serialize)]
        struct JsonError {
            code: i32,
            message: &'static str,
        }

        let headers = HeaderMap::new();

        let (status, body) = match self {
            Error::Internal => (
                StatusCode::INTERNAL_SERVER_ERROR,
                JsonError {
                    code: 2000,
                    message: "Unknown server error",
                },
            ),
            Error::InvalidRequest => (
                StatusCode::FORBIDDEN,
                JsonError {
                    code: 2003,
                    message: "Invalid request",
                },
            ),
            Error::NotFound => (
                StatusCode::NOT_FOUND,
                JsonError {
                    code: 2004,
                    message: "File not found",
                },
            ),
            Error::BookExists => (
                StatusCode::PAYMENT_REQUIRED,
                JsonError {
                    code: 2005,
                    message: "Book is already registered",
                },
            ),
        };

        (status, headers, Json(body)).into_response()
    }
}
pub type Result<T> = std::result::Result<T, Error>;


pub fn get_router(pool: SqlitePool) -> Router {
    Router::new()
        .route("/documents", get(get_documents))
        .route("/documents/cover/:document", get(get_document_cover))
        .route("/documents/epub/:document", get(get_document_epub))
        .route("/documents/new", put(upload_epub))
        .route("/documents/new", post(upload_epub))
        .layer(DefaultBodyLimit::disable())
        .route_layer(middleware::from_fn_with_state(pool.clone(), auth::authorize))
        .with_state(pool)
}


async fn get_documents(
    State(pool): State<SqlitePool>,
) -> Result<impl IntoResponse> {

    #[derive(sqlx::FromRow, Debug)]
    struct Row {
        rowid: i32,
        title: String,
        category: Option<String>,
        summary: Option<String>,
        authors: Option<sqlx::types::Json<Vec<String>>>,
        epub: Option<String>,
        external_link: Option<String>,
        timestamp: i64,
    }

    let records = sqlx::query_as::<_, Row>(
        r#"
SELECT
    rowid, title, category, summary, authors, epub, external_link, timestamp
FROM
    documents
        "#,
    )
    .fetch_all(&pool)
    .await
    .map_err(|_| Error::Internal)?;

    info!(ndocs = records.len(), "Listing documents");

    let feed = atom_syndication::FeedBuilder::default()
        .title("Private OPDS System")
        .entries(records.iter().map(|record| {
            let native = NaiveDateTime::from_timestamp_opt(record.timestamp, 0).unwrap();
            let dt = DateTime::<Utc>::from_utc(native, Utc);

            let mut entry = EntryBuilder::default()
                .id(record.rowid.to_string())
                .title(record.title.clone())
                .updated(dt)
                .build();

            let mut links = vec![];
            if let Some(_epub) = &record.epub {
                links.push(
                    LinkBuilder::default()
                        .rel("http://opds-spec.org/image")
                        .mime_type(Some(String::from("image/png")))
                        .href(format!("/documents/cover/{0}", record.rowid))
                        .build()
                );
                links.push(
                    LinkBuilder::default()
                        .rel("http://opds-spec.org/acquisition")
                        .mime_type(Some(String::from("application/epub+zip")))
                        .title(Some(String::from("EPUB file")))
                        .href(format!("/documents/epub/{0}", record.rowid))
                        .build()
                );
            }

            if let Some(external_link) = &record.external_link {
                links.push(
                    LinkBuilder::default()
                        .rel("alternate")
                        .title(Some(String::from("External link")))
                        .href(external_link)
                        .build()
                );
            }
            entry.set_links(links);

            if let Some(authors) = &record.authors {
                entry.set_authors(authors.iter().map(
                        |author| PersonBuilder::default().name(author).build()
                    ).collect::<Vec<Person>>());
            }

            if let Some(category) = &record.category {
                entry.set_categories(vec![Category {
                        term: category.clone(),
                        scheme: Some(String::from("http://www.bisg.org/standards/bisac_subject/index.html")),
                        label: None
                    }]);
            }

            if let Some(summary) = &record.summary {
                entry.set_summary(Some(TextBuilder::default().value(summary.clone()).build()));
            }

            entry
        }).collect::<Vec<Entry>>())
        .build();

    Ok((
        StatusCode::OK,
        feed.to_string()
    ))
}

async fn find_epub_path(
    pool: SqlitePool,
    document_id: String,
) -> Result<String> {
    #[derive(sqlx::FromRow, Debug)]
    struct Row {
        epub: Option<String>
    }

    let record = sqlx::query_as::<_, Row>(
        r#"
SELECT
    epub
FROM
    documents
WHERE
    rowid = ?1
        "#,
    )
    .bind(&document_id)
    .fetch_optional(&pool)
    .await
    .map_err(|_| Error::Internal)?
    .ok_or(Error::NotFound)?;

    Ok(record.epub.ok_or(Error::NotFound)?)
}

async fn get_document_cover(
    State(pool): State<SqlitePool>,
    Path(document_id): Path<String>,
) -> Result<impl IntoResponse> {

    info!(document_id = document_id, "fetching cover");

    let path = find_epub_path(pool, document_id).await?;
    let mut doc = EpubDoc::new(path).map_err(|_| Error::InvalidRequest)?;
    let (buf, mime) = doc.get_cover().ok_or(Error::NotFound)?;

    let bytes = Bytes::from(buf);
    let body = Full::new(bytes);

    let mut headers = HeaderMap::new();
    headers.insert("content-type", mime.parse().unwrap());

    Ok((
        StatusCode::OK,
        headers,
        body
    ))
}

async fn get_document_epub(
    State(pool): State<SqlitePool>,
    Path(document_id): Path<String>,
) -> Result<impl IntoResponse> {

    info!(document_id = document_id, "fetching epub");

    let path = find_epub_path(pool, document_id).await?;
    let file = match tokio::fs::File::open(path).await {
        Ok(file) => file,
        Err(_) => return Err(Error::NotFound),
    };
    let stream = ReaderStream::new(file);
    let body = StreamBody::new(stream);
    let mut headers = HeaderMap::new();
    headers.insert("content-type", "application/epub+zip".parse().unwrap());

    Ok((
        StatusCode::OK,
        headers,
        body
    ))
}

async fn upload_epub(
    State(pool): State<SqlitePool>,
    // Json(input): Json<BookUploadInput>,
    mut multipart: Multipart
) -> Result<impl IntoResponse> {
    let mut summary: Option<String> = None;
    let mut category: Option<String> = None;
    let mut filename: Option<String> = None;
    let mut ext_link: Option<String> = None;

    while let Some(field) = multipart.next_field().await.unwrap() {
        let name = field.name().unwrap().to_string();
        let file_name = field.file_name().unwrap_or("").to_string();
        let data = field.bytes().await.map_err(|_| Error::Internal)?;
        info!(name=name, file_name=file_name, "parse field");

        if name == "summary" {
            // TODO this isn't great..
            summary = Some(data.escape_ascii().to_string());
            info!(summary=summary, "parsed field");
        } else if name == "category" {
            category = Some(data.escape_ascii().to_string());
            info!(category=category, "parsed field");
        } else if name == "ext_link" {
            ext_link = Some(data.escape_ascii().to_string());
            info!(ext_link=ext_link, "parsed field");
        } else if name == "book" {
            info!("attempt upload");
            let mut fname = String::from("books/");
            fname.push_str(&file_name);
            filename = Some(fname.clone());
            let mut file = File::create(fname).await.map_err(|_| Error::Internal)?;
            file.write_all(&data.clone()).await.map_err(|_| Error::Internal)?;
            file.flush().await.map_err(|_| Error::Internal)?;
            info!(filename=file_name, "Uploaded book");
        }
    }

    if let Some(filename) = filename {
        let timestamp: i64 = Utc::now().timestamp();

        let doc = EpubDoc::new(filename.clone()).map_err(|_| Error::InvalidRequest)?;

        let title: String = match doc.metadata.get("title"){
            Some(title) => title[0].clone(),
            None => filename.clone()
        };

        let authors: Option<String> = match doc.metadata.get("creator") {
            Some(authors) => Some(
                serde_json::to_string(authors).unwrap()
            ),
            None => None
        };
        let language: Option<String> = match doc.metadata.get("language") {
            Some(lang) => Some(lang[0].clone()),
            None => None
        };

        sqlx::query(r#"INSERT INTO documents (title, category, summary, authors, language, epub, external_link, timestamp) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)"#)
            .bind(&title)
            .bind(&category)
            .bind(&summary)
            .bind(&authors)
            .bind(&language)
            .bind(&filename)
            .bind(&ext_link)
            .bind(&timestamp)
            .execute(&pool)
            .await
            .map_err(|e| match e {
                sqlx::Error::Database(de) if de.code().as_deref() == Some("1555") => Error::BookExists,
                _ => Error::Internal,
            })?;
    }

    Ok((StatusCode::OK,""))
}


