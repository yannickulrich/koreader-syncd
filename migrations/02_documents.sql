CREATE TABLE "documents" (
	"title"	TEXT NOT NULL UNIQUE,
	"category"	TEXT,
	"summary"	TEXT,
	"authors"	TEXT,
	"language"	TEXT,
	"epub"	TEXT,
    "external_link" TEXT,
	"timestamp"	INTEGER NOT NULL DEFAULT 0,
	PRIMARY KEY("title")
)
